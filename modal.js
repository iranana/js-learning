
/*
This is a constructor function, invoked using the 'new' keyword. Constructors
are used to create 'object instances'. Objects are like real life objects, with
properties and behaviours.

This constructor takes a settings paramter, in the form of an object. To create
new instances of a Modal, call like so:

var warningModal = new Modal({
  title: 'Warning!',
  text: 'Some one set us up the bomb'.
});

var successModal = new Modal({
  title: 'Success!',
  text: 'Some one diffused us up the bomb'.
});
*/
function Modal (settings) {
  this.title = settings.title;
  this.text = settings.text;

  this.container = document.createElement('div');
  this.content = document.createElement('div');
  this.closeButton = document.createElement('a');

  this.container.classList.add('modal-container');
  this.container.classList.add('modal-content');

  this.closeButton.textContent = 'Close';
  this.closeButton.classList.add('modal-close');

  // When Modal is invoked, bind handlers to elements and then create the modal.
  this.bindHandlers();
  this.create();
}

/*
JS is a prototype based language, meaning that objects inherit directly from other objects.
All objects have a 'prototype' property, which is a linkage back to the object that created
it. By extending the Modal prototype, each object instance of Modal will have access
to these methods. In this context, 'this' will always refer back to the instance.
*/
Modal.prototype.bindHandlers = function () {

  // Add 'click' event listener to the close button.
  // Note the call to the bind() method.
  // This creates a new function call whose 'this' value is set to a particular value.
  // In this case, it's the object instance, so we can call its close() method.
  // In ES6 JS, this isn't necessary due to the addition of the fat arrow method.
  this.closeButton.addEventListener('click', function () {
    this.close();
  }.bind(this));
}

Modal.prototype.create = function () {
  // Build DOM and append to body.
  this.content.innerHTML += '<h1>' + this.title + '</h1>';
  this.content.innerHTML += '<p>' + this.text + '</p>';
  this.content.appendChild(this.closeButton);

  this.container.appendChild(this.content);
  document.body.appendChild(this.container);
}

Modal.prototype.close = function () {
  // Traverse to parent of this.container (document.body), and remove this.container.
  this.container.parentNode.removeChild(this.container);
}

/*
Make our Modal available (extend window object, so that it's callable in the browser).
*/
window.Modal = Modal;
